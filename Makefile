tag=""

docker:
	docker build --no-cache -t registry.gitlab.com/python-utils1/webservice_foundation:${tag} .
	docker push registry.gitlab.com/python-utils1/webservice_foundation:${tag}

testpypi:
	rm -f -r webservice_foundation.egg-info/* dist/* build/*
	python setup.py sdist bdist_wheel
	twine upload --repository testpypi dist/*

pypi:
	rm -f -r webservice_foundation.egg-info/* dist/* build/*
	python setup.py sdist bdist_wheel
	twine upload --repository pypi dist/*
