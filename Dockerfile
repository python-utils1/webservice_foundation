FROM ncsnozominishinohara/python:3.7.4

ENV FILE=webservice.yml
ENV PORT=8080
ENV TZ=Asia/Tokyo
# ENV LANG=ja_JP.UTF-8
ARG USERNAME=webservice
ARG USER_UID=1000
ARG USER_GID=$USER_UID


LABEL maintainer="nozomi nisninohara < nozomi_nishinohara@n-creativesystem.com >"
COPY run.sh /usr/bin

RUN set -e \
    && mkdir -p /usr/app/src \
    && chmod +x /usr/bin/run.sh \
    && pip install -U pip

WORKDIR /usr/app/src

CMD [ "run.sh" ]
