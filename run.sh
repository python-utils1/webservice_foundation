#!/usr/bin/env bash

postgres_init () {
    echo
    local f
    for f; do
        echo "$fを実行"
        psql -h "$POSTGRES_HOST" -U "$POSTGRES_USER" -v ON_ERROR_STOP=1 --quiet "$POSTGRES_DB" < $f
    done
}

mysql_init () {
    echo
    local f
    for f; do
        echo "$fを実行"
        mysql -u ${MYSQL_USER} --password="${MYSQL_PASSWORD}" -h $MYSQL_HOST $MYSQL_DATABASE < $f
    done
}
WAITCNT=10
MIGRATION=${MIGRATION:-'n'}
DATABASE_FILEPATH=${DATABASE_FILEPATH:-'/database/init.d'}
if [[ ${MIGRATION,,} = 'y' ]]; then
    case "${MIGRATION_DATABASE:-}" in
        'POSTGRES' )
            apt-get install postgresql-client -y
            export PGPASSWORD=$POSTGRES_PASSWORD
            cnt=0
            while [ $WAITCNT -gt $cnt ]
            do
                nc -z $POSTGRES_HOST ${POSTGRES_PORT:-5432}
                WAITFORIT_result=$?
                if [ $WAITFORIT_result -eq 0 ]; then
                    break
                fi
                sleep 10
                cnt=$((cnt + 1))
            done
            postgres_init $DATABASE_FILEPATH/*.sql
        ;;
        'MYSQL' )
            apt-get -y install mysql-client
            while [ $WAITCNT -gt $cnt ]
            do
                nc -z $MYSQL_HOST ${MYSQL_PORT:-3306}
                WAITFORIT_result=$?
                if [ $WAITFORIT_result -eq 0 ]; then
                    break
                fi
                sleep 10
                cnt=$((cnt + 1))
            done
            mysql_init $DATABASE_FILEPATH/*.sql
        ;;
    esac
fi

exec miniweb-runner -f $FILE -p $PORT