from webservice_foundation.baserequest import BaseRequestHandler
import json


def get_hello(self: BaseRequestHandler):
    data = {
        'setting': self.settings['google_oauth']['key']
    }
    self.write(json.dumps(data))


def post_hello(self: BaseRequestHandler):
    self.write(json.dumps(self.bodydata[self.BODY]))
