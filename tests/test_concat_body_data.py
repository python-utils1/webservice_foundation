import unittest
from webservice_foundation.bodydata import ConcatBodyData, BodyData
import logging


class pycolor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    END = '\033[0m'
    BOLD = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE = '\033[07m'


class TestLogger(logging.StreamHandler):

    def emit(self, record):
        record.msg = pycolor.RED + record.getMessage() + pycolor.END
        super().emit(record)


class TestConcatBodyData(unittest.TestCase):

    def setUp(self):
        self.t1 = 'body'
        self.t2 = 'body_args_test'
        self.t3 = 'query_args_test'

        self.data = ConcatBodyData({
            'body': BodyData({
                'test': self.t1
            }),
            'body_args': BodyData({
                'test2': self.t2
            }),
            'query_args': BodyData({
                'test3': self.t3
            })
        })
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.stream = TestLogger()
        self.logger.addHandler(self.stream)
        logging.root.handlers = []

    def tearDown(self):
        self.logger.removeHandler(self.stream)

    def test_1(self):
        self.logger.info('bodyに存在')
        data = self.data['test']
        self.assertEqual(self.t1, data)

    def test_2(self):
        self.logger.info('body_argsに存在')
        data = self.data['test2']
        self.assertEqual(self.t2, data)

    def test_3(self):
        self.logger.info('query_argsに存在')
        data = self.data['test3']
        self.assertEqual(self.t3, data)

    def test_4(self):
        self.logger.info('いずれにも存在なし')
        data = self.data['aaaa']
        self.assertEqual(None, None)

    def test_5(self):
        self.logger.info('bodyに存在 get')
        data = self.data.get('test')
        self.assertEqual(self.t1, data)

    def test_6(self):
        self.logger.info('body_argsに存在 get')
        data = self.data.get('test2')
        self.assertEqual(self.t2, data)

    def test_7(self):
        self.logger.info('query_argsに存在 get')
        data = self.data.get('test3')
        self.assertEqual(self.t3, data)

    def test_8(self):
        self.logger.info('いずれにも存在なし get')
        data = self.data.get('aaaa')
        self.assertEqual(None, None)

    def test_9(self):
        self.logger.info('bodyに存在 get第二引数あり')
        data = self.data.get('test', 'aaa')
        self.assertEqual(self.t1, data)

    def test_a(self):
        self.logger.info('body_argsに存在 get第二引数あり')
        data = self.data.get('test2', 'aaa')
        self.assertEqual(self.t2, data)

    def test_b(self):
        self.logger.info('query_argsに存在 get第二引数あり')
        data = self.data.get('test3', 'aaa')
        self.assertEqual(self.t3, data)

    def test_c(self):
        self.logger.info('いずれにも存在なし get第二引数あり')
        data = self.data.get('google_oauth', 'aaa')
        self.assertEqual('aaa',  data)
