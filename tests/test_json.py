import json
import logging
import os

from tornado.testing import AsyncHTTPTestCase
from webservice_foundation.webservice import WebService


class pycolor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    END = '\033[0m'
    BOLD = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE = '\033[07m'


class TestLogger(logging.StreamHandler):

    def emit(self, record):
        record.msg = pycolor.RED + record.getMessage() + pycolor.END
        super().emit(record)


class TestJson(AsyncHTTPTestCase):

    def setUp(self):
        super(TestJson, self).setUp()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.stream_handler = TestLogger()
        self.logger.addHandler(self.stream_handler)
        logging.root.handlers = []

    def tearDown(self):
        super(TestJson, self).tearDown()
        self.logger.removeHandler(self.stream_handler)

    def get_app(self):
        service = WebService()
        service('example/webservice.json', 8080, start=False)
        return service.app

    def test_0_env(self):
        self.logger.info('Env設定')
        self.assertEqual('localhost', os.environ.get('HOST', ''))
        self.assertEqual('TEST_AAAA', os.environ.get('aaa'))

    def test_a_hello(self):
        self.logger.info('GET /api/hello')
        res = self.fetch('/api/hello', method='GET')
        self.assertEqual(200, res.code)
        self.assertEqual({"setting": "aaaaa"},
                         json.loads(res.body.decode('utf-8')))

    def test_b_hello(self):
        data = {
            'key': 'value'
        }
        self.logger.info('POST /api/hello')
        res = self.fetch('/api/hello', method='POST', body=json.dumps(data), headers={
            'Content-Type': 'application/json'
        })
        self.assertEqual(200, res.code)
        data2 = json.loads(res.body)
        self.assertEqual(data, data2)

    def test_c_hello(self):
        self.logger.info('GET /api/test')
        res = self.fetch('/api/test', method='GET')
        self.assertEqual(200, res.code)
        self.assertEqual('TEST GET', res.body.decode('utf-8'))
